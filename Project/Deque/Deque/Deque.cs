﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Drawing;

namespace Deque
{
    class Deque
    {
        DChangeLeftTag ChangeLeftTag;
        DPaintLeft PaintStackLeft;
        DPaintRight PaintStackRight;
        DChangeRightTag ChangeRightTag;
        DPaintLocal PaintStackLocal;
        DPaintDeque PaintDeque;
        DChangeStatus ChangeStatus;

        DIsPause IsPause;

        Stack<Label> leftStack;
        Stack<Label> rightStack;
        Stack<Label> localStack;

        public int sleep = 1200;
        int sleepStop = 5000;

        public Deque(DChangeLeftTag ChangeLeftTag, DPaintLeft PaintLeft, 
                     DChangeRightTag ChangeRightTag, DPaintRight PaintRight,
                     DPaintLocal PaintLocal, DPaintDeque PaintDeque,
                     DChangeStatus ChangeStatus, DIsPause IsPause
            )
        {
            leftStack = new Stack<Label>();
            rightStack = new Stack<Label>();
            localStack = new Stack<Label>();

            this.ChangeLeftTag = ChangeLeftTag;
            PaintStackLeft = PaintLeft;
            this.ChangeRightTag = ChangeRightTag;
            PaintStackRight = PaintRight;
            PaintStackLocal = PaintLocal;
            this.PaintDeque = PaintDeque;
            this.ChangeStatus = ChangeStatus;
            this.IsPause = IsPause;
        }

        public int Count
        {
            get
            {
                return leftStack.Count + rightStack.Count;
            }

            private set { }
        }

        public Label Front
        {
            get
            {
                if (rightStack.Count != 0)
                {
                    return rightStack.ElementAt(0);
                }
                else
                {
                    if (leftStack.Count != 0)
                    {
                        return leftStack.ElementAt(leftStack.Count - 1);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            private set { }
        }

        public Label Back
        {
            get
            {
                if (leftStack.Count != 0)
                {
                    return leftStack.ElementAt(0);
                }
                else
                {
                    if (rightStack.Count != 0)
                    {
                        return rightStack.ElementAt(rightStack.Count - 1);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            private set { }
        }

        public bool Empty
        {
            get
            {
                return leftStack.Count + rightStack.Count == 0;
            }

            private set { }
        }


        public void PushBack(Label item) 
        {
            if (IsPause())
            {
                Thread.Sleep(sleepStop);
            }

            leftStack.Push(item);
            if (rightStack.Count > 0)
                rightStack.ElementAt(0).BackColor = Color.Gold;

            ChangeTags();
            PaintStackLeft(leftStack);
            PaintStackRight(rightStack);
            PaintDeque(leftStack, rightStack, SearchPosition.Back);

            ChangeStatus("PushBack(" + Convert.ToString(item.Tag) + ")");
            Thread.Sleep(sleep);
            ChangeStatus("Can work");
        }

        public Label PopBack()
        {
            Label item;

            // если есть елементы
            if (leftStack.Count != 0)
            {
                leftStack.ElementAt(0).BackColor = Color.Red;
                PaintStackLeft(leftStack);
                PaintDeque(leftStack, rightStack, SearchPosition.Back);
                ChangeStatus("PopBack {" + Convert.ToString(leftStack.ElementAt(0).Tag) + "}");


                if (IsPause())
                {
                    Thread.Sleep(sleepStop);
                }
                Thread.Sleep(sleep);
                leftStack.ElementAt(0).BackColor = Color.Gold;

                item = leftStack.Pop();

                ChangeTags();
                PaintStackLeft(leftStack);
                PaintDeque(leftStack, rightStack, SearchPosition.Back);
                ChangeStatus("Can work");

                return item;
            }

            // проверка на наличие елементов во втором стеке
            if (rightStack.Count == 0)
            {
                MessageBox.Show("Deque is empty!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }

            int size = rightStack.Count;
            // перекидываем половину в локальный стек
            for (int i = 0; i < size / 2; ++i)
            {
                if (IsPause())
                {
                    Thread.Sleep(sleepStop);
                }
                Label curr = rightStack.Pop();
                localStack.Push(curr);
                ChangeStatus("rightStack -> localStack {" + Convert.ToString(curr.Tag) + "}");

                PaintStackLocal(localStack);
                PaintStackRight(rightStack);
                Thread.Sleep(sleep);
            }
            // оставшуюся часть перекидываем в левый стек
            while (rightStack.Count != 0)
            {
                if (IsPause())
                {
                    Thread.Sleep(sleepStop);
                }

                Label curr = rightStack.Pop();
                leftStack.Push(curr);
                ChangeStatus("rightStack -> leftStack {" + Convert.ToString(curr.Tag) + "}");

                PaintStackLeft(leftStack);
                PaintStackRight(rightStack);
                Thread.Sleep(sleep);
            }
            // с локального стека перекидываем в правый стек
            while (localStack.Count != 0)
            {
                if (IsPause())
                {
                    Thread.Sleep(sleepStop);
                }

                Label curr = localStack.Pop();
                rightStack.Push(curr);
                ChangeStatus("localStack -> rightStack {" + Convert.ToString(curr.Tag) + "}");

                PaintStackRight(rightStack);
                PaintStackLocal(localStack);
                Thread.Sleep(sleep);
            }


            if (IsPause())
            {
                Thread.Sleep(sleepStop);
            }

            leftStack.ElementAt(0).BackColor = Color.Red;
            PaintStackLeft(leftStack);
            PaintDeque(leftStack, rightStack, SearchPosition.Front);
            ChangeStatus("PopBack {" + Convert.ToString(leftStack.ElementAt(0).Tag) + "}");
            Thread.Sleep(sleep);
            leftStack.ElementAt(0).BackColor = Color.Gold;

            item = leftStack.Pop();

            ChangeTags();
            PaintStackLeft(leftStack);
            PaintStackRight(rightStack);
            PaintDeque(leftStack, rightStack, SearchPosition.Back);

            ChangeStatus("Can work");

            return item;
        }

        public void PushFront(Label item)
        {
            if (IsPause())
            {
                Thread.Sleep(sleepStop);
            }

            rightStack.Push(item);
            if (leftStack.Count > 0)
                leftStack.ElementAt(0).BackColor = Color.Gold;

            ChangeTags();
            PaintStackRight(rightStack);
            PaintStackLeft(leftStack);
            PaintDeque(leftStack, rightStack, SearchPosition.Front);
            ChangeStatus("PushFront(" + Convert.ToString(item.Tag) + ")");
            Thread.Sleep(sleep);
            ChangeStatus("Can work");
        }

        public Label PopFront()
        {
            Label item; 

            // если есть елементы
            if (rightStack.Count != 0)
            {
                if (IsPause())
                {
                    Thread.Sleep(sleepStop);
                }
                rightStack.ElementAt(0).BackColor = Color.Red;
                PaintStackRight(rightStack);
                PaintDeque(leftStack, rightStack, SearchPosition.Front);
                ChangeStatus("PopFront {" + Convert.ToString(rightStack.ElementAt(0).Tag) + "}");
                Thread.Sleep(sleep);
                ChangeStatus("Can work");
                rightStack.ElementAt(0).BackColor = Color.Gold;

                item = rightStack.Pop();

                ChangeTags();
                PaintStackRight(rightStack);
                PaintDeque(leftStack, rightStack, SearchPosition.Front);

                return item;
            }

            // проверка на наличие елементов во втором стеке
            if (leftStack.Count == 0)
            {
                MessageBox.Show("Deque is empty!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }


            int size = leftStack.Count;
            // половину из левого стека передаем в локальный
            for (int i = 0; i < size / 2; ++i)
            {
                if (IsPause())
                {
                    Thread.Sleep(sleepStop);
                }
                Label curr = leftStack.Pop();
                localStack.Push(curr);

                ChangeStatus("leftStack -> localStack {" + Convert.ToString(curr.Tag) + "}");

                PaintStackLeft(leftStack);
                PaintStackLocal(localStack);
                Thread.Sleep(sleep);
                ChangeStatus("Can work");
            }
            // оставшуюся часть из левого стека передаем в правый
            while (leftStack.Count != 0)
            {
                if (IsPause())
                {
                    Thread.Sleep(sleepStop);
                }

                Label curr = leftStack.Pop();
                rightStack.Push(curr);

                ChangeStatus("leftStack -> rightStack {" + Convert.ToString(curr.Tag) + "}");

                PaintStackLeft(leftStack);
                PaintStackRight(rightStack);
                Thread.Sleep(sleep);
                ChangeStatus("Can work");
            }
            // елементы из локального стека переносим в левый стек
            while (localStack.Count != 0)
            {
                if (IsPause())
                {
                    Thread.Sleep(sleepStop);
                }

                Label curr = localStack.Pop();
                leftStack.Push(curr);

                ChangeStatus("localStack -> leftStack {" + Convert.ToString(curr.Tag) + "}");

                PaintStackLeft(leftStack);
                PaintStackLocal(localStack);
                Thread.Sleep(sleep);
                ChangeStatus("Can work");
            }

            if (IsPause())
            {
                Thread.Sleep(sleepStop);
            }

            rightStack.ElementAt(0).BackColor = Color.Red;
            PaintStackRight(rightStack);
            PaintDeque(leftStack, rightStack, SearchPosition.Front);
            ChangeStatus("PopBack {" + Convert.ToString(rightStack.ElementAt(0).Tag) + "}");
            Thread.Sleep(sleep);
            rightStack.ElementAt(0).BackColor = Color.Gold;

            item = rightStack.Pop();
            ChangeTags();
            PaintStackLeft(leftStack);
            PaintStackRight(rightStack);
            PaintDeque(leftStack, rightStack, SearchPosition.Front);

            ChangeStatus("Can work");

            return item;
        }

        public void Clear()
        {
            leftStack.Clear();
            rightStack.Clear();
        }
        // -----------------------------------------------------------------------

        private void ChangeTags()
        {
            ChangeLeftTag(leftStack.Count);
            ChangeRightTag(rightStack.Count);
        }
    }
}
