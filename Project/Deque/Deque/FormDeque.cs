﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace Deque
{
    public delegate void DChangeLeftTag(int itemsCount);
    public delegate void DPaintLeft(Stack<Label> itemsLeft);
    public delegate void DChangeRightTag(int itemsCount);
    public delegate void DPaintRight(Stack<Label> itemsRight);
    public delegate void DPaintLocal(Stack<Label> itemsLocal);
    public delegate void DPaintDeque(Stack<Label> itemsLeft, Stack<Label> itemsRight, SearchPosition position);
    public delegate void DChangeStatus(string text);
    public delegate bool DIsPause();



    public enum SearchPosition {
        Front,
        Back
    }

    public partial class FormDeque : Form
    {
        Deque deque;
        const int SIZE = 49;
        const int DURATION = 2000;
        bool pause = false;

        Label GetNode(Label node) {
            Label newNode = new Label();
            newNode.Size = node.Size;
            newNode.Text = node.Text;
            newNode.BackColor = node.BackColor;
            newNode.BorderStyle = node.BorderStyle;
            newNode.TextAlign = node.TextAlign;

            newNode.Tag = node.Tag;
            newNode.MouseEnter += ShowInfoLable;

            return newNode;
        }        

        public FormDeque()
        {
            InitializeComponent();
            InitMainPanel();
            panelStackRight.AutoScroll = true;
            deque = new Deque(ChangeLeftTag, PaintLeft, ChangeRightTag, PaintRight, PaintLocal, PaintDeque, ChangeStatus, IsPause);
        }

        private void FormDeque_SizeChanged(object sender, EventArgs e)
        {
            InitMainPanel();
        }

        private string FormatText(string text)
        {
            string format = "";

            if (text.Length > 5)
            {
                format = text.Substring(0, 4);
                format += "...";
            }
            else
            {
                format = text;
            }

            return format;
        }

        private void InitMainPanel()
        {
            int x, y;

            x = (this.Width - panelMain.Width) / 2;
            y = (this.Height - panelMain.Height - labelStatus.Height) / 2;

            panelMain.Location = new Point(x, y);
        }

        private bool IsCorrectInput()
        {
            if (textInput.Text == "")
            {
                DialogResult dr = MessageBox.Show("You didn't write the input. Do you want put null?", "Warning", 
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                if (dr == System.Windows.Forms.DialogResult.OK)
                {
                    textInput.Text = "null";
                    return true;
                }
                if (dr == System.Windows.Forms.DialogResult.Cancel)
                {
                    return false;
                }
            }

            return true;
        }

        private void ShowInfoLable(object sender, EventArgs e)
        {
            string text = Convert.ToString(((Label)sender).Tag);
            int x = Convert.ToInt32(((Label)sender).Width) / 2;
            int y = Convert.ToInt32(((Label)sender).Height) / 2;


            toolTip.Show(text, (Label)sender, x, y, DURATION);
        }

        public Label CreateItem(string data, int itemsCount, Color color)
        {
            Label item = new Label();
            item.Text = data;
            item.Height = SIZE;
            item.Width = SIZE;
            item.TextAlign = ContentAlignment.MiddleCenter;
            item.BackColor = color;
            item.BorderStyle = BorderStyle.FixedSingle;
            item.Location = new Point(panelStackLeft.Size.Width - (itemsCount + 1) * SIZE, 0);

            return item;
        }

        public void PaintLeft(Stack<Label> itemsLeft)
        {
            if (panelStackLeft.Width < itemsLeft.Count * SIZE)
            {
                panelStackLeft.Controls.Clear();

                for (int i = itemsLeft.Count - 1; i >= 0; --i)
                {
                    itemsLeft.ElementAt(i).Dock = DockStyle.Left;
                    if (i != 0)
                    {
                        itemsLeft.ElementAt(i).BackColor = Color.Gold;
                    }
                    
                    panelStackLeft.Controls.Add(itemsLeft.ElementAt(i));
                }
            }
            else
            {
                panelStackLeft.Controls.Clear();

                int lastCoordX = panelStackLeft.Width - SIZE;
                for (int i = itemsLeft.Count - 1; i >= 0; --i)
                {
                    if (i != 0)
                    {
                        itemsLeft.ElementAt(i).BackColor = Color.Gold;
                    }

                    itemsLeft.ElementAt(i).Dock = DockStyle.None;
                    itemsLeft.ElementAt(i).Location = new Point(lastCoordX - SIZE * (itemsLeft.Count - 1 - i), itemsLeft.ElementAt(i).Location.Y);
                    panelStackLeft.Controls.Add(itemsLeft.ElementAt(i));
                }
            }

            panelStackLeft.Refresh();
        }

        private void buttonPushBack_Click(object sender, EventArgs e)
        {
            if (!IsCorrectInput())
            {
                return;
            }

            Label item = CreateItem(textInput.Text, Convert.ToInt32(panelStackLeft.Tag), Color.Green);
            item.Text = FormatText(item.Text);
            item.Tag = textInput.Text;
            item.MouseEnter += ShowInfoLable;
            deque.PushBack(item);

            textInput.Focus();
            panelResult.Controls.Clear();
        }

        public void ChangeLeftTag(int itemsCount)
        {
            panelStackLeft.Tag = itemsCount;
        }

        private void buttonPopBack_Click(object sender, EventArgs e)
        {
            panelDeque.AutoScrollPosition = new Point(panelDeque.Location.X, (deque.Count) * SIZE);
            Label item = deque.PopBack();

            if (item == null) return;

            item.Dock = DockStyle.Fill;
            item.Font = new System.Drawing.Font(FontFamily.GenericSerif, 19, FontStyle.Bold);
            panelResult.Controls.Clear();
            panelResult.Controls.Add(item);
            panelResult.Refresh();

            textInput.Focus();
        }

        // ------------------------------------------------------------------

        public void PaintRight(Stack<Label> itemsRight)
        {
            if (panelStackRight.Width < itemsRight.Count * SIZE)
            {
                panelStackRight.Controls.Clear();

                for (int i = 0; i < itemsRight.Count; ++i)
                {
                    if (i != 0)
                    {
                        itemsRight.ElementAt(i).BackColor = Color.Gold;
                    }

                    itemsRight.ElementAt(i).Dock = DockStyle.Left;
                    panelStackRight.Controls.Add(itemsRight.ElementAt(i));
                    panelStackRight.AutoScrollPosition = new Point(i * SIZE, panelStackRight.Location.Y);
                }
            }
            else
            {
                panelStackRight.Controls.Clear();

                int lastCoordX = 0;

                for (int i = itemsRight.Count - 1; i >= 0; --i)
                {
                    if (i != 0)
                    {
                        itemsRight.ElementAt(i).BackColor = Color.Gold;
                    }

                    itemsRight.ElementAt(i).Dock = DockStyle.None;
                    itemsRight.ElementAt(i).Location = new Point(lastCoordX + (itemsRight.Count - i - 1) * SIZE, itemsRight.ElementAt(i).Location.Y);
                    panelStackRight.Controls.Add(itemsRight.ElementAt(i));
                }
            }

            panelStackRight.Refresh();
        }

        public void ChangeRightTag(int itemsCount)
        {
            panelStackRight.Tag = itemsCount;
        }

        private void buttonPushFront_Click(object sender, EventArgs e)
        {
            if (!IsCorrectInput())
            {
                return;
            }

            Label item = CreateItem(textInput.Text, Convert.ToInt32(panelStackRight.Tag), Color.Green);
            item.Text = FormatText(item.Text);
            item.Tag = textInput.Text;
            item.MouseEnter += ShowInfoLable;

            deque.PushFront(item);

            textInput.Focus();
            panelResult.Controls.Clear();
        }

        private void buttonPopFront_Click(object sender, EventArgs e)
        {
            Label item = deque.PopFront();

            if (item == null) return;

            item.Dock = DockStyle.Fill;
            item.Font = new System.Drawing.Font(FontFamily.GenericSerif, 20, FontStyle.Bold);
            panelResult.Controls.Clear();
            panelResult.Controls.Add(item);
            panelResult.Refresh();

            textInput.Focus();
        }

        // ------------------------------------------------------------------

        public void PaintLocal(Stack<Label> itemsLocal)
        {
            if (panelStackLocal.Width < itemsLocal.Count * SIZE)
            {
                panelStackLocal.Controls.Clear();

                for (int i = itemsLocal.Count - 1; i >= 0; --i)
                {
                    itemsLocal.ElementAt(i).Dock = DockStyle.Left;
                    panelStackLocal.Controls.Add(itemsLocal.ElementAt(i));
                }
            }
            else
            {
                panelStackLocal.Controls.Clear();

                int lastCoordX = panelStackLocal.Width - SIZE;

                for (int i = itemsLocal.Count - 1; i >= 0; --i)
                {
                    itemsLocal.ElementAt(i).Dock = DockStyle.None;
                    itemsLocal.ElementAt(i).Location = new Point(lastCoordX - (itemsLocal.Count - i - 1) * SIZE, itemsLocal.ElementAt(i).Location.Y);
                    panelStackLocal.Controls.Add(itemsLocal.ElementAt(i));
                }
            }

            panelStackLocal.Refresh();
        }

        // ------------------------------------------------------------------

        public void PaintDeque(Stack<Label> itemsLeft, Stack<Label> itemsRight, SearchPosition position)
        {
            int leftCount = itemsLeft.Count;
            int rightCount = itemsRight.Count;

            panelDeque.Controls.Clear();

            // left stack
            for (int i = 0; i < leftCount; ++i)
            {
                Label item = GetNode(itemsLeft.ElementAt(i));
                item.Dock = DockStyle.Top;
                panelDeque.Controls.Add(item);
            }

            // right stack
            for (int i = rightCount - 1; i >= 0; --i)
            {
                Label item = GetNode(itemsRight.ElementAt(i));
                item.Dock = DockStyle.Top;
                panelDeque.Controls.Add(item);
            }

            if (position == SearchPosition.Back)
            {
                panelDeque.AutoScrollPosition = new Point(panelDeque.Location.X, (leftCount + rightCount + 1) * SIZE);
            }

            panelDeque.Refresh();
        }

        // ------------------------------------------------------------------

        public void ChangeStatus(string text)
        {
            labelStatus.Text = text;
            labelStatus.Refresh();
        }

        private void menu_examples_test1_Click(object sender, EventArgs e)
        {
            Random rand = new Random();

            for (int i = 0; i < 10; ++i)
            {
                textInput.Text = rand.Next(-100000, 100000).ToString();
                buttonPushBack_Click(null, null);
            }

            for (int i = 0; i < 10; ++i)
            {
                textInput.Text = rand.Next(-100000, 100000).ToString();
                buttonPushFront_Click(null, null);
            }

            for (int i = 0; i < 10; ++i)
            {
                // textInput.Text = rand.Next(-100000, 100000).ToString();
                buttonPopBack_Click(null, null);
            }

            for (int i = 0; i < 10; ++i)
            {
                // textInput.Text = rand.Next(-100000, 100000).ToString();
                buttonPopFront_Click(null, null);
            }

        }

        private void menu_examples_test2_Click(object sender, EventArgs e)
        {
            Random rand = new Random();

            for (int i = 0; i < 10; ++i)
            {
                textInput.Text = rand.Next(-100000, 100000).ToString();
                buttonPushBack_Click(null, null);
                buttonPopFront_Click(null, null);
            }

            buttonPopBack_Click(null, null);
        }

        private void menu_examples_test3_Click(object sender, EventArgs e)
        {
            Random rand = new Random();

            for (int i = 0; i < 10; ++i)
            {
                textInput.Text = rand.Next(-100000, 100000).ToString();
                buttonPushBack_Click(null, null);
            }

            for (int i = 0; i < 10; ++i)
            {
                buttonPopFront_Click(null, null);
            }

        }

        private void menu_examples_test4_Click(object sender, EventArgs e)
        {
            Random rand = new Random();

            for (int i = 0; i < 10; ++i)
            {
                textInput.Text = rand.Next(-100000, 100000).ToString();
                buttonPushFront_Click(null, null);
            }

            for (int i = 0; i < 10; ++i)
            {
                buttonPopBack_Click(null, null);
            }
        }

        private void menu_examples_test5_Click(object sender, EventArgs e)
        {
            Random rand = new Random();

            for (int i = 0; i < 5; ++i)
            {
                textInput.Text = rand.Next(-100000, 100000).ToString();
                buttonPushBack_Click(null, null);

                buttonPopBack_Click(null, null);
                buttonPopBack_Click(null, null);
            }

        }

        private void menu_speed_half_Click(object sender, EventArgs e)
        {
            deque.sleep = 500;
        }

        private void menu_speed_second_Click(object sender, EventArgs e)
        {
            deque.sleep = 1000;
        }

        private void menu_speed_onehalf_second_Click(object sender, EventArgs e)
        {
            deque.sleep = 1500;
        }

        private void menu_start_Click(object sender, EventArgs e)
        {
            pause = true;
        }

        private void menu_stop_Click(object sender, EventArgs e)
        {
            pause = false;
        }

        private bool IsPause()
        {
            return pause;
        }
    }
}
