﻿namespace Deque
{
    partial class FormDeque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDeque));
            this.panelStackLeft = new System.Windows.Forms.Panel();
            this.panelStackLocal = new System.Windows.Forms.Panel();
            this.panelStackRight = new System.Windows.Forms.Panel();
            this.BorderStackLeft = new System.Windows.Forms.PictureBox();
            this.borderStackLocal = new System.Windows.Forms.PictureBox();
            this.borderStackRight = new System.Windows.Forms.PictureBox();
            this.textInput = new System.Windows.Forms.TextBox();
            this.buttonPushBack = new System.Windows.Forms.Button();
            this.buttonPopBack = new System.Windows.Forms.Button();
            this.buttonPushFront = new System.Windows.Forms.Button();
            this.buttonPopFront = new System.Windows.Forms.Button();
            this.panelResult = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panelDeque = new System.Windows.Forms.Panel();
            this.pictureDequeBorder = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panelMain = new System.Windows.Forms.Panel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.labelStatus = new System.Windows.Forms.Label();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.menu_examples = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_examples_test1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_examples_test2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_examples_test3 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_examples_test4 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_examples_test5 = new System.Windows.Forms.ToolStripMenuItem();
            this.speedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_speed_half = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_speed_second = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_speed_onehalf_second = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.BorderStackLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderStackLocal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderStackRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureDequeBorder)).BeginInit();
            this.panelMain.SuspendLayout();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelStackLeft
            // 
            this.panelStackLeft.AutoScroll = true;
            this.panelStackLeft.Location = new System.Drawing.Point(25, 48);
            this.panelStackLeft.Name = "panelStackLeft";
            this.panelStackLeft.Size = new System.Drawing.Size(333, 65);
            this.panelStackLeft.TabIndex = 0;
            // 
            // panelStackLocal
            // 
            this.panelStackLocal.AutoScroll = true;
            this.panelStackLocal.Location = new System.Drawing.Point(25, 158);
            this.panelStackLocal.Name = "panelStackLocal";
            this.panelStackLocal.Size = new System.Drawing.Size(333, 67);
            this.panelStackLocal.TabIndex = 1;
            // 
            // panelStackRight
            // 
            this.panelStackRight.AutoScroll = true;
            this.panelStackRight.Location = new System.Drawing.Point(27, 270);
            this.panelStackRight.Name = "panelStackRight";
            this.panelStackRight.Size = new System.Drawing.Size(333, 67);
            this.panelStackRight.TabIndex = 2;
            // 
            // BorderStackLeft
            // 
            this.BorderStackLeft.BackColor = System.Drawing.Color.Black;
            this.BorderStackLeft.Location = new System.Drawing.Point(27, 46);
            this.BorderStackLeft.Name = "BorderStackLeft";
            this.BorderStackLeft.Size = new System.Drawing.Size(333, 69);
            this.BorderStackLeft.TabIndex = 3;
            this.BorderStackLeft.TabStop = false;
            // 
            // borderStackLocal
            // 
            this.borderStackLocal.BackColor = System.Drawing.Color.Black;
            this.borderStackLocal.Location = new System.Drawing.Point(27, 156);
            this.borderStackLocal.Name = "borderStackLocal";
            this.borderStackLocal.Size = new System.Drawing.Size(333, 71);
            this.borderStackLocal.TabIndex = 4;
            this.borderStackLocal.TabStop = false;
            // 
            // borderStackRight
            // 
            this.borderStackRight.BackColor = System.Drawing.Color.Black;
            this.borderStackRight.Location = new System.Drawing.Point(25, 268);
            this.borderStackRight.Name = "borderStackRight";
            this.borderStackRight.Size = new System.Drawing.Size(333, 71);
            this.borderStackRight.TabIndex = 5;
            this.borderStackRight.TabStop = false;
            // 
            // textInput
            // 
            this.textInput.Location = new System.Drawing.Point(750, 47);
            this.textInput.Name = "textInput";
            this.textInput.Size = new System.Drawing.Size(136, 20);
            this.textInput.TabIndex = 7;
            // 
            // buttonPushBack
            // 
            this.buttonPushBack.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonPushBack.Location = new System.Drawing.Point(750, 105);
            this.buttonPushBack.Name = "buttonPushBack";
            this.buttonPushBack.Size = new System.Drawing.Size(136, 44);
            this.buttonPushBack.TabIndex = 8;
            this.buttonPushBack.Text = "Push back";
            this.buttonPushBack.UseVisualStyleBackColor = true;
            this.buttonPushBack.Click += new System.EventHandler(this.buttonPushBack_Click);
            // 
            // buttonPopBack
            // 
            this.buttonPopBack.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonPopBack.Location = new System.Drawing.Point(750, 165);
            this.buttonPopBack.Name = "buttonPopBack";
            this.buttonPopBack.Size = new System.Drawing.Size(136, 44);
            this.buttonPopBack.TabIndex = 9;
            this.buttonPopBack.Text = "Pop back";
            this.buttonPopBack.UseVisualStyleBackColor = true;
            this.buttonPopBack.Click += new System.EventHandler(this.buttonPopBack_Click);
            // 
            // buttonPushFront
            // 
            this.buttonPushFront.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonPushFront.Location = new System.Drawing.Point(750, 238);
            this.buttonPushFront.Name = "buttonPushFront";
            this.buttonPushFront.Size = new System.Drawing.Size(136, 44);
            this.buttonPushFront.TabIndex = 10;
            this.buttonPushFront.Text = "Push front";
            this.buttonPushFront.UseVisualStyleBackColor = true;
            this.buttonPushFront.Click += new System.EventHandler(this.buttonPushFront_Click);
            // 
            // buttonPopFront
            // 
            this.buttonPopFront.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonPopFront.Location = new System.Drawing.Point(750, 299);
            this.buttonPopFront.Name = "buttonPopFront";
            this.buttonPopFront.Size = new System.Drawing.Size(136, 44);
            this.buttonPopFront.TabIndex = 11;
            this.buttonPopFront.Text = "Pop front";
            this.buttonPopFront.UseVisualStyleBackColor = true;
            this.buttonPopFront.Click += new System.EventHandler(this.buttonPopFront_Click);
            // 
            // panelResult
            // 
            this.panelResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelResult.Location = new System.Drawing.Point(414, 121);
            this.panelResult.Name = "panelResult";
            this.panelResult.Size = new System.Drawing.Size(94, 86);
            this.panelResult.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(28, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 13;
            this.label1.Text = "Left stack";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(30, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Local stack";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(29, 248);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 16);
            this.label3.TabIndex = 15;
            this.label3.Text = "Right stack";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(411, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 16);
            this.label4.TabIndex = 16;
            this.label4.Text = "Pop result";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(747, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 16);
            this.label5.TabIndex = 17;
            this.label5.Text = "Input:";
            // 
            // panelDeque
            // 
            this.panelDeque.AutoScroll = true;
            this.panelDeque.Location = new System.Drawing.Point(563, 46);
            this.panelDeque.Name = "panelDeque";
            this.panelDeque.Size = new System.Drawing.Size(85, 288);
            this.panelDeque.TabIndex = 18;
            // 
            // pictureDequeBorder
            // 
            this.pictureDequeBorder.BackColor = System.Drawing.Color.Black;
            this.pictureDequeBorder.Location = new System.Drawing.Point(561, 46);
            this.pictureDequeBorder.Name = "pictureDequeBorder";
            this.pictureDequeBorder.Size = new System.Drawing.Size(89, 288);
            this.pictureDequeBorder.TabIndex = 19;
            this.pictureDequeBorder.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(564, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 16);
            this.label6.TabIndex = 20;
            this.label6.Text = "Deque";
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.label4);
            this.panelMain.Controls.Add(this.panelResult);
            this.panelMain.Controls.Add(this.panelStackLeft);
            this.panelMain.Controls.Add(this.label6);
            this.panelMain.Controls.Add(this.panelDeque);
            this.panelMain.Controls.Add(this.BorderStackLeft);
            this.panelMain.Controls.Add(this.panelStackLocal);
            this.panelMain.Controls.Add(this.label5);
            this.panelMain.Controls.Add(this.pictureDequeBorder);
            this.panelMain.Controls.Add(this.label3);
            this.panelMain.Controls.Add(this.panelStackRight);
            this.panelMain.Controls.Add(this.label2);
            this.panelMain.Controls.Add(this.buttonPopFront);
            this.panelMain.Controls.Add(this.label1);
            this.panelMain.Controls.Add(this.buttonPushFront);
            this.panelMain.Controls.Add(this.borderStackLocal);
            this.panelMain.Controls.Add(this.buttonPopBack);
            this.panelMain.Controls.Add(this.borderStackRight);
            this.panelMain.Controls.Add(this.buttonPushBack);
            this.panelMain.Controls.Add(this.textInput);
            this.panelMain.Location = new System.Drawing.Point(12, 36);
            this.panelMain.Margin = new System.Windows.Forms.Padding(0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(911, 357);
            this.panelMain.TabIndex = 21;
            // 
            // labelStatus
            // 
            this.labelStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelStatus.BackColor = System.Drawing.Color.Black;
            this.labelStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelStatus.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelStatus.ForeColor = System.Drawing.Color.Green;
            this.labelStatus.Location = new System.Drawing.Point(12, 411);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(911, 32);
            this.labelStatus.TabIndex = 21;
            this.labelStatus.Text = "Can work";
            this.labelStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_examples,
            this.speedToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(934, 24);
            this.menu.TabIndex = 22;
            this.menu.Text = "menuStrip1";
            // 
            // menu_examples
            // 
            this.menu_examples.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_examples_test1,
            this.menu_examples_test2,
            this.menu_examples_test3,
            this.menu_examples_test4,
            this.menu_examples_test5});
            this.menu_examples.Name = "menu_examples";
            this.menu_examples.Size = new System.Drawing.Size(68, 20);
            this.menu_examples.Text = "Examples";
            // 
            // menu_examples_test1
            // 
            this.menu_examples_test1.Name = "menu_examples_test1";
            this.menu_examples_test1.Size = new System.Drawing.Size(152, 22);
            this.menu_examples_test1.Text = "Test 1";
            this.menu_examples_test1.Click += new System.EventHandler(this.menu_examples_test1_Click);
            // 
            // menu_examples_test2
            // 
            this.menu_examples_test2.Name = "menu_examples_test2";
            this.menu_examples_test2.Size = new System.Drawing.Size(152, 22);
            this.menu_examples_test2.Text = "Test 2";
            this.menu_examples_test2.Click += new System.EventHandler(this.menu_examples_test2_Click);
            // 
            // menu_examples_test3
            // 
            this.menu_examples_test3.Name = "menu_examples_test3";
            this.menu_examples_test3.Size = new System.Drawing.Size(152, 22);
            this.menu_examples_test3.Text = "Test 3";
            this.menu_examples_test3.Click += new System.EventHandler(this.menu_examples_test3_Click);
            // 
            // menu_examples_test4
            // 
            this.menu_examples_test4.Name = "menu_examples_test4";
            this.menu_examples_test4.Size = new System.Drawing.Size(152, 22);
            this.menu_examples_test4.Text = "Test 4";
            this.menu_examples_test4.Click += new System.EventHandler(this.menu_examples_test4_Click);
            // 
            // menu_examples_test5
            // 
            this.menu_examples_test5.Name = "menu_examples_test5";
            this.menu_examples_test5.Size = new System.Drawing.Size(152, 22);
            this.menu_examples_test5.Text = "Test 5";
            this.menu_examples_test5.Click += new System.EventHandler(this.menu_examples_test5_Click);
            // 
            // speedToolStripMenuItem
            // 
            this.speedToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_speed_half,
            this.menu_speed_second,
            this.menu_speed_onehalf_second});
            this.speedToolStripMenuItem.Name = "speedToolStripMenuItem";
            this.speedToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.speedToolStripMenuItem.Text = "Speed";
            // 
            // menu_speed_half
            // 
            this.menu_speed_half.Name = "menu_speed_half";
            this.menu_speed_half.Size = new System.Drawing.Size(152, 22);
            this.menu_speed_half.Text = "0.5 second";
            this.menu_speed_half.Click += new System.EventHandler(this.menu_speed_half_Click);
            // 
            // menu_speed_second
            // 
            this.menu_speed_second.Name = "menu_speed_second";
            this.menu_speed_second.Size = new System.Drawing.Size(152, 22);
            this.menu_speed_second.Text = "1 second";
            this.menu_speed_second.Click += new System.EventHandler(this.menu_speed_second_Click);
            // 
            // menu_speed_onehalf_second
            // 
            this.menu_speed_onehalf_second.Name = "menu_speed_onehalf_second";
            this.menu_speed_onehalf_second.Size = new System.Drawing.Size(152, 22);
            this.menu_speed_onehalf_second.Text = "1.5 second";
            this.menu_speed_onehalf_second.Click += new System.EventHandler(this.menu_speed_onehalf_second_Click);
            // 
            // FormDeque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 452);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menu;
            this.MinimumSize = new System.Drawing.Size(950, 448);
            this.Name = "FormDeque";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Deque";
            this.SizeChanged += new System.EventHandler(this.FormDeque_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.BorderStackLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderStackLocal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderStackRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureDequeBorder)).EndInit();
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelStackLeft;
        private System.Windows.Forms.Panel panelStackLocal;
        private System.Windows.Forms.Panel panelStackRight;
        private System.Windows.Forms.PictureBox BorderStackLeft;
        private System.Windows.Forms.PictureBox borderStackLocal;
        private System.Windows.Forms.PictureBox borderStackRight;
        private System.Windows.Forms.TextBox textInput;
        private System.Windows.Forms.Button buttonPushBack;
        private System.Windows.Forms.Button buttonPopBack;
        private System.Windows.Forms.Button buttonPushFront;
        private System.Windows.Forms.Button buttonPopFront;
        private System.Windows.Forms.Panel panelResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panelDeque;
        private System.Windows.Forms.PictureBox pictureDequeBorder;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem menu_examples;
        private System.Windows.Forms.ToolStripMenuItem menu_examples_test1;
        private System.Windows.Forms.ToolStripMenuItem menu_examples_test2;
        private System.Windows.Forms.ToolStripMenuItem menu_examples_test3;
        private System.Windows.Forms.ToolStripMenuItem menu_examples_test4;
        private System.Windows.Forms.ToolStripMenuItem menu_examples_test5;
        private System.Windows.Forms.ToolStripMenuItem speedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menu_speed_half;
        private System.Windows.Forms.ToolStripMenuItem menu_speed_second;
        private System.Windows.Forms.ToolStripMenuItem menu_speed_onehalf_second;
    }
}

