#include <iostream>
#include <stack>
#include <string>
#include <fstream>

using std::stack;
using std::string;
using std::size_t;
using std::cin;
using std::cout;

std::ifstream input("Files/deque.in");
std::ofstream output("Files/deque.out");

struct Stack {
	Stack() {
		index = 0;
		size_stack = 5;
		datas = new string[5];
	}

	size_t Size();
	bool Empty();
	void Push(string item);
	string Pop();
	string At(size_t ind);

	void Clear();

private:
	string *datas;
	int index;
	int size_stack;
};

struct Deque {

	void PushBack(string item);
	string PopBack();
	void PushFront(string item);
	string PopFront();

	string Front();
	string Back();

	size_t Size();
	void Print(std::ostream & out);
	void PrintCounts(std::ostream & out);
	void Clear();

private:
	Stack leftStack;
	Stack rightStack;
	Stack localSrack;
};

// -------------------------------
void do_query(std::istream & in, std::ostream & out);

void print_help(std::ostream & out);

int main() {
	do_query(input, output);

	return 0;
}

void do_query(std::istream & in, std::ostream & out) {
	string command_push_back = "push_back";
	string command_push_front = "push_front";
	string command_pop_back = "pop_back";
	string command_pop_front = "pop_front";

	string command_front = "front";
	string command_back = "back";
	string command_size = "size";
	string command_print = "print";
	string command_counts = "counts";
	string command_help = "help";

	string command_clear = "clear";
	string command_exit = "exit";

	string query;
	string ok = "ok";
	Deque dequeue;
	while (true) {
		in >> query;
		if (query == command_push_back) {
			string item;
			in >> item;
			dequeue.PushBack(item);
			out << ok << std::endl;
		}
		else if (query == command_pop_back){
			string last = dequeue.PopBack();
			out << last << std::endl;
		}
		else if (query == command_push_front) {
			string item;
			in >> item;
			dequeue.PushFront(item);
			out << ok << std::endl;
		}
		else if (query == command_pop_front) {
			string first = dequeue.PopFront();
			out << first << std::endl;
		}
		else if (query == command_front) {
			string first = dequeue.Front();
			out << first << std::endl;
		}
		else if (query == command_back) {
			string last = dequeue.Back();
			out << last << std::endl;
		}
		else if (query == command_size) {
			out << dequeue.Size() << std::endl;
		}
		else if (query == command_clear) {
			dequeue.Clear();
			out << ok << std::endl;
		}
		else if (query == command_print) {
			dequeue.Print(out);
			out << std::endl;
		}
		else if (query == command_counts) {
			dequeue.PrintCounts(out);
		}
		else if (query == command_exit) {
			out << "bye" << std::endl;

			return;
		}
		else if (query == command_help) {
			print_help(out);
			out << std::endl;
		}
		else {
			out << "'" + query + "'" + " not found." << std::endl;
			out << "Write 'help' for reading about commands.";
			out << std::endl;
		}
	}
}

void print_help(std::ostream & out) {
	string command_push_back = "push_back";
	string command_push_front = "push_front";
	string command_pop_back = "pop_back";
	string command_pop_front = "pop_front";

	string command_front = "front";
	string command_back = "back";
	string command_size = "size";
	string command_print = "print";
	string command_counts = "counts";
	string command_clear = "clear";
	string command_exit = "exit";

	out << "push_back  - this command add new element to the end of the deque" << std::endl;
	out << "push_front - this command add new element to the head of the deque" << std::endl;
	out << "pop_back   - this command delete last element from the deque" << std::endl;
	out << "pop_front  - this command delete first element from the deque" << std::endl;

	out << "front      - this command return first element from the deque" << std::endl;
	out << "back       - this command return last element from the deque" << std::endl;
	out << "size       - this command return size of the deque" << std::endl;
	out << "print      - this command print deque's elemenets from head to tail" << std::endl;
	out << "counts     - this command print size of the deque and the stacks" << std::endl;
	out << "clear      - this command delete all elements from the deque" << std::endl;

	out << "exit       - this command close the program" << std::endl;
}

// ----------------------------------------------------
void Deque::PushBack(string item) {
	this->leftStack.Push(item);
}

string Deque::PopBack() {
	string error = "error! The deque has not any element";

	if (!this->leftStack.Empty()) {
		return this->leftStack.Pop();
	}
	else {
		if (this->rightStack.Empty()) {
			return error;
		}
		else {
			long long sizeToLocal = this->rightStack.Size() / 2;
			for (long long i = 0; i < sizeToLocal; ++i) {
				this->localSrack.Push(this->rightStack.Pop());
			}

			while (!this->rightStack.Empty()) {
				this->leftStack.Push(this->rightStack.Pop());
			}

			while (!this->localSrack.Empty()) {
				this->rightStack.Push(this->localSrack.Pop());
			}


			return this->leftStack.Pop();
		}
	}
}

void Deque::PushFront(string item) {
	this->rightStack.Push(item);
}

string Deque::PopFront() {
	string error = "error! The deque has not any element";

	if (!this->rightStack.Empty()) {
		return this->rightStack.Pop();
	}
	else {
		if (this->leftStack.Empty()) {
			return error;
		}
		else {
			long long  sizeToLocal = this->leftStack.Size() / 2;
			for (long long i = 0; i < sizeToLocal; ++i) {
				this->localSrack.Push(this->leftStack.Pop());
			}

			while (!this->leftStack.Empty()) {
				this->rightStack.Push(this->leftStack.Pop());
			}

			while (!this->localSrack.Empty()) {
				this->leftStack.Push(this->localSrack.Pop());
			}

			return this->rightStack.Pop();
		}
	}
}

string Deque::Front() {
	if (this->rightStack.Size() == 0 && this->leftStack.Size() == 0) {
		return "error";
	}

	if (this->rightStack.Size() != 0) {
		return this->rightStack.At(this->rightStack.Size() - 1);
	}
	else {
		return this->leftStack.At(0);
	}
}

string Deque::Back() {
	if (this->leftStack.Size() == 0 && this->rightStack.Size() == 0) {
		return "error";
	}

	if (this->leftStack.Size() != 0) {
		return this->leftStack.At(this->leftStack.Size() - 1);
	}
	else {
		return this->rightStack.At(0);
	}
}

void Deque::Print(std::ostream & out) {
	out << "Deque (head->tail): " << std::endl;

	if (leftStack.Size() + rightStack.Size() == 0) {
		out << "Deque has not any elements!";
	}
	else {

		// print right stack
		for (int i = rightStack.Size() - 1; i >= 0; --i) {
			out << rightStack.At(i) << " ";
		}
		// print left stack
		for (int i = 0; i < leftStack.Size(); ++i) {
			out << leftStack.At(i) << " ";
		}
	}

	out << std::endl << std::endl;
}

void Deque::PrintCounts(std::ostream & out) {
	out << "Count of left stack: " << leftStack.Size() << std::endl;
	out << "Count of right stack: " << rightStack.Size() << std::endl;
	out << "Count of deque: " << this->Size();

	out << std::endl << std::endl;
}

size_t Deque::Size() {
	return this->leftStack.Size() + this->rightStack.Size();
}

void Deque::Clear() {
	while (!this->leftStack.Empty()) {
		this->leftStack.Pop();
	}

	while (!this->rightStack.Empty()) {
		this->rightStack.Pop();
	}
}

// ----------------------------------------------------
size_t Stack::Size() {
	return index;
}

void Stack::Push(string item) {
	if (this->index == this->size_stack) {
		int new_size_stack = 2 * size_stack / 3 + size_stack;
		string *newDatas = new string[new_size_stack];
		for (int i = 0; i < size_stack; ++i) {
			newDatas[i] = datas[i];
		}
		delete[] datas;
		datas = newDatas;
		this->size_stack = new_size_stack;
	}

	datas[index] = item;
	index++;
}

string Stack::Pop() {
	index--;
	return datas[index];
}

string Stack::At(size_t ind) {
	return this->datas[ind];
}

bool Stack::Empty() {
	return this->index == 0;
}

void Stack::Clear() {
	delete datas;
	datas = new string[5];
	size_stack = 5;
	index = 0;
}
